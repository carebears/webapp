/*
 * Copyright 2014 Aarhus University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var app = angular.module('openHealthApp', [
    'ngRoute',
    'ui.bootstrap',
    'openHealthApp.userSignup',
    'openHealthApp.userSignin',
    'openHealthApp.home',
    'openHealthApp.authentication',
    'openHealthApp.configuration',
    'openHealthApp.measurement',
    'openHealthApp.patient',
    'openHealthApp.profile',
    'ui.gravatar',
    'openHealthApp.log',
    'openHealthApp.about'
]);

app.config(['$routeProvider', '$httpProvider',
    function($routeProvider, $httpProvider) {
        $routeProvider.otherwise({
            redirectTo: '/home'
        });

        $httpProvider.interceptors.push('authenticationInterceptorService');
    }
]);

app.run(['authenticationService',
    function(authenticationService) {
        authenticationService.fillAuthData();
        authenticationService.fillUserData();
    }
]);

app.controller('IndexCtrl', ['$scope', '$location', 'authenticationService',
    function($scope, $location, authenticationService) {

        $scope.signOut = function() {
            authenticationService.signOut();
            $location.path('/home');
        };

        $scope.authentication = authenticationService.authentication;
        $scope.user = authenticationService.user;
    }
]);

angular.module('ui.gravatar').config([
  'gravatarServiceProvider', function(gravatarServiceProvider) {
    gravatarServiceProvider.defaults = {
      "default": 'mm'  // Mystery man as default for missing avatars
    };

    // Use https endpoint
    //gravatarServiceProvider.secure = true;
  }
]);