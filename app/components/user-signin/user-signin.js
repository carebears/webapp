/*
 * Copyright 2014 Aarhus University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var appModule = angular.module('openHealthApp.userSignin', ['ngRoute', 'openHealthApp.authentication']);

appModule.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when('/signin', {
            templateUrl: 'components/user-signin/user-signin.html',
            controller: 'UserSigninCtrl'
        });
    }
]);

appModule.controller('UserSigninCtrl', ['$scope', '$location', 'authenticationService',
    function($scope, $location, authenticationService) {
        $scope.signinData = {
            userName: "",
            password: ""
        };

        $scope.message = "";

        $scope.signin = function() {

            authenticationService.signIn($scope.signinData).then(function(response) {

                    $location.path('/');

                },
                function(err) {
                    $scope.message = err.error_description;
                });
        };
    }
]);