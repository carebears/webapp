/*
 * Copyright 2014 Aarhus University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var appModule = angular.module('openHealthApp.measurement', ['ngRoute', 'ui.bootstrap', 'openHealthApp.configuration', 'googlechart','openHealthApp.patient']);

appModule.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when('/measurements/:patientId?', {
            templateUrl: 'components/measurement/measurements.html',
            controller: 'MeasurementsCtrl'
        });
    }
]);

appModule.controller('MeasurementsCtrl', ['$scope', '$modal', '$routeParams', 'measurementManager', 'measurementChartDrawService', 'authenticationService', 'patientManager',
    function($scope, $modal, $routeParams, measurementManager, measurementChartDrawService, authenticationService, patientManager) {

        
        /* Scope properties */
        $scope.measurements = [];
        $scope.measurementTypes = measurementManager.getMeasurementTypes();
        $scope.isChartCollapsed = false;
        $scope.selectedMeasurementType = $scope.measurementTypes[0];
        $scope.patientIdParam = $routeParams.patientId;
        $scope.authentication = authenticationService.authentication;
        $scope.user = authenticationService.user;
        $scope.patientViewing = {};

        /* Scope methods */
        $scope.updateTable = function() {
            measurementManager.loadAllMeasurements($scope.selectedMeasurementType.Type, $scope.patientIdParam).then(function(measurementsData) {
                $scope.measurements = measurementsData;
                $scope.chartObject = measurementChartDrawService.createChartData(measurementsData, $scope.selectedMeasurementType);
            });
        };

        $scope.toggleCollapse = function() {
            $scope.isChartCollapsed = !$scope.isChartCollapsed;
            $scope.chartObject = measurementChartDrawService.createChartData($scope.measurements, $scope.selectedMeasurementType);
        };

        $scope.setTab = function(newValue) {
            $scope.selectedMeasurementType = newValue;
            $scope.updateTable();
        };

        $scope.isSet = function(tabName) {
            return $scope.selectedMeasurementType === tabName;
        };

        $scope.setSelectedMeasurement = function() {
            $scope.selectedMeasurement = this.measurement;
        };

        $scope.newMeasurement = function() {
            $scope.modalInstance = $modal.open({
                templateUrl: 'components/measurement/measurement-new.html',
                controller: 'NewMeasurementCtrl',
                resolve: {
                    measurementTypes: function() {
                        return angular.copy($scope.measurementTypes);
                    },
                    selectedMeasurementIndex : function() {
                        return $scope.measurementTypes.indexOf($scope.selectedMeasurementType);
                    }
                }
            });
            $scope.modalInstance.result.then(function(newMeasurement) {
                //Success
                measurementManager.createMeasurement(newMeasurement, $scope.patientIdParam).then(function() {
                    $scope.updateTable();
                });
            }, function() {
                //Dismissed
            })['finally'](function() {
                $scope.modalInstance = undefined;
            });
        };

        $scope.modifyMeasurement = function(measurement, measurementType) {
            $scope.modalInstance = $modal.open({
                templateUrl: 'components/measurement/measurement-modify.html',
                controller: 'ModifyMeasurementCtrl',
                resolve: {
                    measurementToModify: function() {
                        return measurement;
                    },
                    measurementType: function() {
                        return  $scope.selectedMeasurementType.Type;
                    }
                }
            });
            $scope.modalInstance.result.then(function(modifiedMeasurement) {
                measurementManager.modifyMeasurement(modifiedMeasurement, measurementType).then(function() {
                    $scope.updateTable();
                });
            })['finally'](function() {
                $scope.modalInstance = undefined;
            });
        };

        $scope.deleteMeasurement = function(measurement, measurementType) {
            $scope.modalInstance = $modal.open({
                templateUrl: 'components/measurement/measurement-delete.html',
                controller: 'DeleteMeasurementCtrl'
            });
            $scope.modalInstance.result.then(function() {
                measurementManager.deleteMeasurement(measurement, measurementType).then(function() {
                    $scope.updateTable();
                });
            })['finally'](function() {
                $scope.modalInstance = undefined;
            });
        };

        /* Initialization */
        if($scope.user.role === 'healthcareprofessional') {
            patientManager.viewTrackedPatientProfile($scope.patientIdParam).then(function(patientObj){
            $scope.patientViewing = patientObj;
        });}
        
        $scope.updateTable();
    }
]);

appModule.controller('NewMeasurementCtrl', ['$scope', '$modalInstance', 'measurementTypes', 'selectedMeasurementIndex',
    function($scope, $modalInstance, measurementTypes, selectedMeasurementIndex) {
        
        $scope.defaultMeasurementIndex = selectedMeasurementIndex;
        $scope.measurementTypes = measurementTypes;
        $scope.newMeasurement = measurementTypes[selectedMeasurementIndex];

        $scope.open = function($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.opened = true;
        };

        $scope.today = function() {
            $scope.newMeasurement.Date = new Date();
        };

        $scope.ok = function() {
            $modalInstance.close($scope.newMeasurement);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.today();
    }
]);

appModule.controller('DeleteMeasurementCtrl', ['$scope', '$modalInstance',
    function($scope, $modalInstance) {
        $scope.ok = function() {
            $modalInstance.close();
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);

appModule.controller('ModifyMeasurementCtrl', ['$scope', '$modalInstance', 'measurementToModify', 'measurementType',
    function($scope, $modalInstance, measurementToModify, measurementType) {

        $scope.measurementType = angular.copy(measurementType);
        $scope.measurement = angular.copy(measurementToModify);

        /* Hack, to enable date to be loaded on start */
        $scope.measurement.Date = new Date($scope.measurement.Date);

        $scope.ok = function() {
            $modalInstance.close($scope.measurement);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);

appModule.factory('Measurement', ['$http', 'configurationService',
    function($http, configurationService) {
        var measurementId = 1;

        function Measurement(measurementData) {
            if (measurementData) {
                this.setData(measurementData);
            }

            measurementId = measurementData.id;
        }

        Measurement.prototype = {
            setData: function(measurementData) {
                angular.extend(this, measurementData);
            }
        };

        return Measurement;
    }
]);

appModule.factory('measurementManager', ['$http', '$q', 'configurationService', 'Measurement',
    function($http, $q, configurationService, Measurement) {
        var measurementManager = {
            _pool: {},
            _retrieveInstance: function(measurementId, measurementData) {
                var instance = this._pool[measurementId];

                if (instance) {
                    instance.setData(measurementData);
                } else {
                    instance = new Measurement(measurementData);
                    this._pool[measurementId] = instance;
                }

                return instance;
            },
            _search: function(measurementId) {
                return this._pool[measurementId];
            },
            _load: function(measurementId, deferred) {
                var scope = this;

                $http.get(configurationService.serviceBase + 'api/measurements/' + measurementId)
                    .succes(function(measurementData) {
                        var measurement = scope._retrieveInstance(measurementData.id, measurementData);
                        deferred.resolve(measurement);
                    })
                    .errror(function() {
                        deferred.reject();
                    });
            },
            /* Public Methods */
            /* Use this function in order to get a measurement instance by it's id */
            getMeasurement: function(measurementId) {
                var deferred = $q.defer();
                var measurement = this._search(measurementId);
                if (measurement) {
                    deferred.resolve(measurement);
                } else {
                    this._load(measurementId, deferred);
                }

                return deferred.promise;
            },
            /* Used to retrieve measurement types*/
            getMeasurementTypes: function() {

                var weight = {
                    Type: "Weight",
                    Date: '',
                    Id: '',
                    Value: ''
                };

                var bloodGlucose = {
                    Type: "BloodGlucose",
                    Date: '',
                    Id: '',
                    Value: ''
                };

                var bloodPressure = {
                    Type: "BloodPressure",
                    Date: '',
                    Id: '',
                    Systolic: '',
                    Diastolic: '',
                    HeartRate: ''
                };

                var temperature = {
                    Type: "Temperature",
                    Date: '',
                    Id: '',
                    Value: ''
                };

                var saturation = {
                    Type: "Saturation",
                    Date: '',
                    Id: '',
                    Value: '',
                    Heartrate: ''
                };

                var activityEvent = {
                    Type: "ActivityEvent",
                    Date: '',
                    Id: '',
                    Value: ''
                };

                var height = {
                    Type: "Height",
                    Date: '',
                    Id: '',
                    Value: ''
                };

                var spirometer = {
                    Type: "Spirometer",
                    Date: '',
                    Id: '',
                    Value: ''
                };

                var INR = {
                    Type: "INR",
                    Date: '',
                    Id: '',
                    Value: ''
                };

                var respiratoryRate = {
                    Type: "RespiratoryRate",
                    Date: '',
                    Id: '',
                    Value: ''
                };

                var TUG = {
                    Type: "TUG",
                    Date: '',
                    Id: '',
                    Value: ''
                };

                var measurementTypes = [
                    weight,
                    bloodGlucose,
                    bloodPressure,
                    temperature,
                    saturation,
                    activityEvent,
                    height,
                    spirometer,
                    INR,
                    respiratoryRate,
                    TUG
                ];

                return measurementTypes;
            },
            /* Use this function in order to get instances of all measurements */
            loadAllMeasurements: function(type, patientId) {
                var deferred = $q.defer();
                var scope = this;
                var url = configurationService.serviceBase + 'api/measurements/' + type;
                if(patientId !== undefined){
                    url += '/?patientId=' + patientId;
                }
                $http.get(url)
                    .then(function(response) {
                        var measurementsArray = response.data;
                        var measurements = [];
                        for (var i = 0; i < measurementsArray.length; i++) {
                            var measurement = scope._retrieveInstance(measurementsArray[i].Id, measurementsArray[i]);
                            measurements.push(measurement);
                        }
                        /* measurementsArray.for forEach(function(measurementData) {
                         var measurement = scope._retrieveInstance(measurementData.Id, measurementData);
                         measurements.push(measurement);
                         });*/
                        deferred.resolve(measurements);
                    })
                    .then(function() {
                        deferred.reject();
                    });
                return deferred.promise;
            },
            setMeasurement: function(measurementData) {
                var scope = this;
                var measurement = this._search(measurementData.id);
                if (measurement) {
                    measurement.setData(measurementData);
                } else {
                    measurement = scope._retrieveInstance(measurementData);
                }
                return measurement;
            },
            createMeasurement: function(measurementData, patientId) {
                var deferred = $q.defer();
                var scope = this;
                var url = configurationService.serviceBase + 'api/measurements/' + measurementData.Type.toLowerCase();
                if(patientId !== undefined){
                    url += '/?patientId=' + patientId;
                }
                $http.post(url, measurementData)
                    .then(function() {
                        deferred.resolve();
                    })
                    .then(function() {
                        deferred.reject();
                    });
                return deferred.promise;
            },
            deleteMeasurement: function(measurementData, measurementType) {
                var deferred = $q.defer();
                var scope = this;
                var url = configurationService.serviceBase + 'api/measurements/' + measurementType.toLowerCase() + '/' + measurementData.Id;
                
                $http.delete(url)
                    .then(function() {
                        deferred.resolve();
                    })
                    .then(function() {
                        deferred.reject();
                    });
                return deferred.promise;
            },
            modifyMeasurement: function(measurementData, measurementType) {
                var deferred = $q.defer();
                var scope = this;
                var url = configurationService.serviceBase + 'api/measurements/' + measurementType.toLowerCase();
                
                $http.put(url, measurementData)
                    .then(function() {
                        deferred.resolve();
                    })
                    .then(function() {
                        deferred.reject();
                    });
                return deferred.promise;
            }
        };

        return measurementManager;
    }
]);

appModule.factory('measurementChartDrawService', [

    function() {
        var chartService = {
            createChartData: function(measurementData, measurementType) {
                /*Sorting measurements by date*/
                measurementData.sort(function(m1, m2) {
                    m1 = new Date(m1.Date);
                    m2 = new Date(m2.Date);
                    return m1 < m2 ? -1 : m1 > m2 ? 1 : 0;
                });

                var chartObject = {
                    'type': 'AreaChart',
                    'displayed': true,
                    'options': {
                        'fontSize': '14',
                        'fontName': 'Helvetica',
                        'displayExactValues': true,
                        'backgroundColor': '#f5f5f5',
                        'curveType': 'function',
                        'lineWidth': '4',
                        'pointSize': '8',
                        'areaOpacity': '0.1',
                        'chartArea': {
                            'backgroundColor': '#f5f5f5',
                            'width': '90%',
                            'height': '90%'
                        },
                        'legend': {
                            'position': 'bottom'
                        },
                        'animation': {
                            'duration': 10,
                            'easing': 'linear'
                        },
                        'hAxis': {
                            'gridlines.color': '#eee',
                            'textStyle.color': '#666'
                        }
                    },
                    'data': {
                        'cols': [{
                            'id': 'date',
                            'label': 'Date',
                            'type': 'string'
                        }],
                        'rows': []
                    }
                };

                var typeParameters = [];
                var pCount = 0;

                angular.forEach(measurementType, function(value, key) {
                    if (['Type', 'Id', 'Date'].indexOf(key) == -1) {

                        typeParameters[pCount] = key;

                        chartObject.data.cols.push({
                            'id': key.toLowerCase(),
                            'label': key,
                            'type': 'number'
                        });

                        pCount++;
                    }
                });

                var cCount = 0;
                /* Iterate through Measurements */
                angular.forEach(measurementData, function(measurement) {
                    var data = {
                        'c': []
                    };
                    /* Insert date as first datapoint*/
                    var date = new Date(measurement.Date);
                    data.c.push({
                        'v': date.toLocaleString()
                    });
                    /* Iterate through key/values of each measurement, and add to data.
                       Check if key matches with what is defined in measurementType */
                    angular.forEach(measurement, function(value, key) {
                        if (typeParameters.indexOf(key) != -1)
                            data.c[typeParameters.indexOf(key) + 1] = {
                                'v': value
                            };
                    });

                    chartObject.data.rows[cCount] = data;
                    cCount++;
                });

                return chartObject;
            }
        };

        return chartService;
    }
]);