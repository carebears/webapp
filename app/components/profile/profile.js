/*
 * Copyright 2014 Aarhus University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var appModule = angular.module('openHealthApp.profile', ['ngRoute', 'ui.bootstrap', 'openHealthApp.configuration', 'ui.gravatar']);

appModule.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when('/profile', {
            templateUrl: 'components/profile/profile.html',
            controller: 'ProfileCtrl'
        });
    }
]);

appModule.controller('ProfileCtrl', ['$scope', 'profileManager', '$modal',
    function($scope, profileManager, $modal) {

        /* Scope properties */
        $scope.profile = {};
        $scope.trackingHealthcareProfessionals = [];

        /* Scope methods */
        $scope.createNewPassphrase = function() {
            $scope.modalInstance = $modal.open({
                templateUrl: 'components/profile/profile-passphrase.html',
                controller: 'PassphraseCtrl',
                size: 'md'
            });
            $scope.modalInstance.result.then(function(passphrase) {
                profileManager.newPassphrase(passphrase).then(function() {
                    //TODO: Success message

                }).then(function() {
                    //TODO: Error message
                });
            })['finally'](function() {
                $scope.modalInstance = undefined;
            });
        };

        $scope.updateProfile = function() {
            profileManager.loadCurrentProfile()
                .then(function(userProfile) {
                    $scope.profile = userProfile;
                    if ($scope.profile.Roles[0] === 'patient') {
                        profileManager.loadTrackingHealthcareProfessionals()
                            .then(function(trackingHcps) {
                                $scope.trackingHealthcareProfessionals = trackingHcps;
                            });
                    }
                });
        };

        $scope.editProfile = function() {
            $scope.modalInstance = $modal.open({
                templateUrl: 'components/profile/profile-edit.html',
                controller: 'EditProfileCtrl',
                size: 'md',
                resolve: {
                    profileToEdit: $scope.profile
                }
            });
        };
        
        /* Initialization */
        $scope.updateProfile();

    }
]);

appModule.controller('PassphraseCtrl', ['$scope', '$modalInstance',
    function($scope, $modalInstance) {
        $scope.ok = function() {
            $modalInstance.close($scope.passphrase);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);

appModule.controller('EditProfileCtrl', ['$scope', '$modalInstance', 
    function ($scope, $modalInstance) {
        $scope.ok = function() {
            $modalInstance.close($scope.editedProfile);
        };

        $scope.close = function() {
            $modalInstance.dismiss('cancel');
        };
}]);

appModule.factory('profileManager', ['$http', '$q', 'configurationService',
    function($http, $q, configurationService) {
        return {
            loadCurrentProfile: function() {
                var deferred = $q.defer();
                var url = configurationService.serviceBase + 'api/users';
                $http.get(url)
                    .then(function(response) {
                        deferred.resolve(response.data);
                    }).then(function() {
                        deferred.reject();
                    });
                return deferred.promise;
            },
            loadProfile: function(userId) {
                var deferred = $q.defer();
                var url = configurationService.serviceBase + 'api/users/' + userId;
                $http.get(url)
                    .then(function(response) {
                        deferred.resolve(response.data);
                    }).then(function() {
                        deferred.reject();
                    });
                return deferred.promise;
            },
            newPassphrase: function(passphrase) {
                var deferred = $q.defer();
                var url = configurationService.serviceBase + 'api/tracking/patients/passphrase';

                $http.post(url, passphrase).then(function(response) {
                    deferred.resolve();
                }).then(function() {
                    deferred.reject();
                });

                return deferred.promise;
            },
            loadTrackingHealthcareProfessionals: function() {
                var deferred = $q.defer();
                var url = configurationService.serviceBase + 'api/tracking/healthcareprofessionals';

                $http.get(url).then(function(response) {
                    deferred.resolve(response.data);
                }).then(function() {
                    deferred.reject();
                });

                return deferred.promise;
            }
        };
    }
]);