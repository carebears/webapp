/*
 * Copyright 2014 Aarhus University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var appModule = angular.module('openHealthApp.authentication', ['LocalStorageModule', 'openHealthApp.configuration']);

appModule.factory('authenticationService', ['$http', '$q', 'localStorageService', 'configurationService',
    function($http, $q, localStorageService, configurationService) {

        var serviceBase = configurationService.serviceBase;
        var authServiceFactory = {};

        var _authentication = {
            isAuth: false,
            userName: "",
            profileType: ""
        };

        var _user = {
            role: "",
            userName: "",
            givenName: "",
            surName: ""
        };

        var _saveSignUp = function(signUpData, profileType) {

            _signOut();

            return $http.post(serviceBase + 'api/users/' + profileType.toLowerCase(), signUpData).then(function(response) {
                return response;
            });

        };

        var _signIn = function(signInData) {

            var data = "grant_type=password&username=" + signInData.userName + "&password=" + signInData.password;

            var deferred = $q.defer();

            $http.post(serviceBase + 'token', data, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function(response) {

                /* Set user data */
                $http.get(configurationService.serviceBase + "api/users").then(function(response){
                    var user = response.data;
                    
                    _user.role = user.Roles[0];
                    _user.userName = user.UserName;
                    _user.givenName = user.GivenName;
                    _user.surName = user.Surname;

                    localStorageService.set('userData', {
                        role: user.Roles[0],
                        userName: user.UserName,
                        givenName: user.GivenName,
                        surName: user.Surname
                    });
                });

                /* Set authentication data */
                _authentication.isAuth = true;

                localStorageService.set('authorizationData', {
                    token: response.access_token
                });

                deferred.resolve(response);

            }).error(function(err, status) {
                _signOut();
                deferred.reject(err);
            });

            return deferred.promise;

        };

        var _signOut = function() {

            localStorageService.remove('authorizationData');
            localStorageService.remove('userData');

            _authentication.isAuth = false;
            _authentication.userName = "";
            _authentication.profileType = "";

            _user.role = "";
            _user.userName = "";
            _user.givenName = "";
            _user.surName = "";
        };

        var _fillAuthData = function() {

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
                _authentication.profileType = authData.profileType;
            }

        };

        var _fillUserData = function() {
            var userData = localStorageService.get('userData');
            if(userData) {
                _user.role = userData.role;
                _user.userName = userData.userName;
                _user.givenName = userData.givenName;
                _user.surName = userData.surName;

            }
        };

        authServiceFactory.saveSignUp = _saveSignUp;
        authServiceFactory.signIn = _signIn;
        authServiceFactory.signOut = _signOut;
        authServiceFactory.fillAuthData = _fillAuthData;
        authServiceFactory.fillUserData = _fillUserData;
        authServiceFactory.authentication = _authentication;
        authServiceFactory.user = _user;

        return authServiceFactory;
    }
]);

appModule.factory('authenticationInterceptorService', ['$q', '$location', 'localStorageService',
    function($q, $location, localStorageService) {

        var authInterceptorServiceFactory = {};

        var _request = function(config) {

            config.headers = config.headers || {};

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        };

        var _responseError = function(rejection) {
            if (rejection.status === 401) {
                $location.path('/signin');
            }
            return $q.reject(rejection);
        };

        authInterceptorServiceFactory.request = _request;
        authInterceptorServiceFactory.responseError = _responseError;

        return authInterceptorServiceFactory;
    }
]);