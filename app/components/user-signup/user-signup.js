/*
 * Copyright 2014 Aarhus University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var appModule = angular.module('openHealthApp.userSignup', ['ngRoute', 'ui.bootstrap', 'openHealthApp.authentication']);

appModule.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when('/signup', {
            templateUrl: 'components/user-signup/user-signup.html',
            controller: 'UserSignupCtrl'
        });
    }
]);

appModule.controller('UserSignupCtrl', ['$scope', '$http', '$timeout', '$location',
    '$modal', 'authenticationService',
    function($scope, $http, $timeout, $location, $modal, authenticationService) {
        //Genders to select
        $scope.genders = ['Male', 'Female'];
        $scope.selectedProfile = 'Patient';

        //Redirect countdown timer
        $scope.counter = 5;
        var redirectCountdown;
        $scope.countdownRedirect = function(url) {
            redirectCountdown = $timeout(function() {
                $scope.counter--;
                $scope.countdownRedirect(url);
                if ($scope.counter === 0) {
                    $scope.modalInstance.dismiss('redirect');
                    $location.path(url);
                }
            }, 1000);
        };

        //Alert popups
        $scope.alerts = [];
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        //Submit of signup, using authenticationService
        $scope.submitSignup = function(signupUser) {
            if ($scope.signup.password !== $scope.repeatPassword) {
                $scope.alerts.push({
                    type: 'danger',
                    msg: 'The repeated password doesn\'t match.'
                });
            } else {
                signupUser.UserName = signupUser.email;
                authenticationService.saveSignUp(signupUser, $scope.selectedProfile)
                    .then(function(response) {
                            //SignUp success
                            $scope.modalInstance = $modal.open({
                                templateUrl: 'successModal.html',
                                controller: 'UserSignupCtrl'
                            });
                            $scope.countdownRedirect('/signin');
                        },
                        function(response) {
                            var errors = [];
                            for (var key in response.data.modelState) {
                                for (var i = 0; i < response.data.modelState[key].length; i++) {
                                    errors.push(response.data.modelState[key][i]);
                                }
                            }
                            $scope.alerts.push({
                                type: 'danger',
                                msg: 'Failed to sign up user due to: ' + errors.join(' ')
                            });
                        });
            }
        };
    }
]);