/*
 * Copyright 2014 Aarhus University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var appModule = angular.module('openHealthApp.patient', ['ngRoute', 'ui.bootstrap', 'openHealthApp.configuration']);

appModule.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when('/patients', {
            templateUrl: 'components/patient/patients.html',
            controller: 'PatientsCtrl'
        });
    }
]);

appModule.controller('PatientsCtrl', ['$scope', '$modal', '$location', 'patientManager',
    function($scope, $modal, $location, patientManager) {

        var Patient = {
            GivenName: '',
            Surname: '',
            Birthday: '',
            Email: ''
        };

        $scope.setSelectedOrder = function(givenProperty){
            if($scope.propertyOrder === givenProperty){
                $scope.propertyOrder = '-' + givenProperty;
            } else{
                $scope.propertyOrder = givenProperty;
            }
        };

        $scope.alerts = [];
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        patientManager.loadTrackedPatients().then(function(trackedPatients) {
            $scope.trackedPatients = trackedPatients;
        });

        $scope.loadTrackedPatientMeasurements = function(patient){
            $location.path('/measurements/'+ patient.Id);
        };

        $scope.openTrackPatients = function() {
            $scope.modalInstance = $modal.open({
                templateUrl: 'components/patient/patient-track.html',
                controller: 'TrackPatientCtrl',
                size: 'md'
            });
            $scope.modalInstance.result.then(function(patientToTrack) {
                patientManager.postPatientToTrack(patientToTrack).then(function() {
                    patientManager.loadTrackedPatients().then(function(trackedPatients) {
                        $scope.trackedPatients = trackedPatients;
                    });
                }).then(function(responseMsg){
                    // handles responseMsg when no patient found with given email and passphrase
                    $scope.alerts.push({
                        type: 'danger',
                        msg: responseMsg
                    });
                });
            })['finally'](function() {
                $scope.modalInstance = undefined;
            });
        };
        
        $scope.removeTrackedPatient = function(trackedPatient) {
            $scope.modalInstance = $modal.open({
                templateUrl: 'components/patient/patient-untrack.html',
                controller: 'UntrackPatientCtrl'
            });
            $scope.modalInstance.result.then(function() {
                patientManager.removeTrackedPatient(trackedPatient).then(function() {
                    patientManager.loadTrackedPatients().then(function(trackedPatient) {
                        $scope.trackedPatients = trackedPatient;
                    });
                });
            })['finally'](function() {
                $scope.modalInstance = undefined;
            });
        };
    }
]);

appModule.controller('TrackPatientCtrl', ['$scope', '$modalInstance', 'patientManager',
    function($scope, $modalInstance, patientManager) {

        $scope.patient = {
            Email: '',
            PassPhrase: ''
        };

        $scope.ok = function() {
            $modalInstance.close($scope.patient);
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }

]);

appModule.controller('UntrackPatientCtrl',['$scope', '$modalInstance',
    function($scope, $modalInstance) {
        $scope.ok = function() {
            $modalInstance.close();
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    }
]);

appModule.factory('patientManager', ['$http', '$q', 'configurationService',
    function($http, $q, configurationService) {
        var patientManager = {

            /* Use this function in order to get instances of all tracked patients*/
            loadTrackedPatients: function () {
                var deferred = $q.defer();
                var scope = this;
                $http.get(configurationService.serviceBase + 'api/tracking/patients/')
                    .then(function (response) {
                        var patientsArray = response.data;

                        deferred.resolve(patientsArray);
                    })
                    .then(function () {
                        deferred.reject();
                    });
                return deferred.promise;
            },
            /* Use this function in order to post instances of desired patients to track*/
            postPatientToTrack: function (patientData) {
                var deferred = $q.defer();
                var scope = this;
                var url = configurationService.serviceBase + 'api/tracking/patients/?' + 'email=' + patientData.Email + '&passphrase=' + patientData.PassPhrase;
                $http.post(url, patientData)
                    .then(function () {
                        deferred.resolve();
                    })
                    .then(function () {
                        deferred.reject('No patient found with given email and passphrase');
                    });
                return deferred.promise;
            },
            /* Use this function in order to removed a tracked instances of a patient*/
            removeTrackedPatient: function (patientData) {
                var deferred = $q.defer();
                var scope = this;
                var url = configurationService.serviceBase + 'api/tracking/patients/' + patientData.Id;
                $http.delete(url)
                    .then(function () {
                        deferred.resolve();
                    })
                    .then(function () {
                        deferred.reject();
                    });
                return deferred.promise;
            },
            /* Use this function in order to get instances of all tracked patients*/
            viewTrackedPatientProfile: function (patientId) {
                var deferred = $q.defer();
                var scope = this;
                var url = configurationService.serviceBase + 'api/tracking/patients/' + patientId;
                $http.get(url)
                    .then(function (response) {
                        var patientObj = response.data;

                        deferred.resolve(patientObj);
                    })
                    .then(function () {
                        deferred.reject();
                    });
                return deferred.promise;
            }

        };
        return patientManager;
    }
]);
