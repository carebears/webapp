/*
 * Copyright 2014 Aarhus University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var appModule = angular.module('openHealthApp.log', ['ngRoute', 'ui.bootstrap', 'openHealthApp.configuration']);

appModule.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when('/logs', {
            templateUrl: 'components/log/logs.html',
            controller: 'LogsCtrl'
        });
    }
]);

appModule.controller('LogsCtrl', ['$scope', 'logManager',
    function($scope, logManager ) {

        $scope.logItem = {
            1: 'MeasurementType',
            2: 'Action',
            3: 'ModifyedBy',
            4: 'Date'
        };

        $scope.logs = [];

        logManager.loadLogs().then(function(data) {
            $scope.logs = data;
        });
}]);

appModule.factory('logManager', ['$http', '$q', 'configurationService',
    function($http, $q, configurationService) {
        var logManager = {

            /* Use this function in order to get instances of logItems*/
            loadLogs: function() {
                var deferred = $q.defer();
                var scope = this;
                $http.get(configurationService.serviceBase + 'api/logitems')
                    .then(function(response) {
                        var logItemsArray = response.data;

                        deferred.resolve(logItemsArray);
                    })
                    .then(function() {
                        deferred.reject();
                    });
                return deferred.promise;
            }
        };
        return logManager;

    }
]);